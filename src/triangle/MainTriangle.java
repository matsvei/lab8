package triangle;

import triangle.Triangle;

public class MainTriangle {

    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(1, 4, 5);
        triangle1.setA(3);
        triangle1.setB(4);
        triangle1.setC(5);
        display(triangle1);
    }

    public static void display(Triangle triangle1){
        System.out.printf("Sides of triangle are : %f, %f and %f", triangle1.getA(), triangle1.getB(), triangle1.getC());
        System.out.println("\n Perimeter of the triangle is : " + triangle1.findPerimeter());
        System.out.println("Area of the triangle is : " + triangle1.findArea());
    }
}

