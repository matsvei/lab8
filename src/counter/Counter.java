package counter;

class Counter {
    private int min, max, current;

    public Counter(int min, int max, int current) {
        this.min = min;
        this.max = max;
        this.current = current;

        if (max < min) {
            int tmp = max;
            max = min;
            min = tmp;
        }

        if (current < min)
            current = min;
        if (current > max)
            current = max;
    }
    public void inc() {
        current++;
        if (current > max)
            current = min;
    }

    public void dec() {
        current--;
        if (current < min)
            current = max;
    }

    public int value() {
        return current;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public int getCurrent() {
        return current;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public void setCurrent(int current) {
        this.current = current;
    }
}