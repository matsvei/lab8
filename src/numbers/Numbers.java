package numbers;

class Numbers {

    private double firstNumber;

    private double secondNumber;

    public Numbers(double firstNumber, double secondNumber){
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;

    }

    public double findSum(){
        return firstNumber + secondNumber;
    }

    public double findMax(){
        return Math.max(firstNumber, secondNumber);
    }

    public double getFirstNumber() {
        return firstNumber;
    }

    public double getSecondNumber() {
        return secondNumber;
    }

    public void setFirstNumber(double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public void setSecondNumber(double secondNumber) {
        this.secondNumber = secondNumber;
    }
}
